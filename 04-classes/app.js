class Human {
    constructor({name, age, gender}){
        this._name = name;
        this._age = age;
        this._gender = gender;
    }
    static startWork(){
        console.log('Ok, everyone starts to work!');
    }
    startWork(){
        console.log(`Ok, ${this._name} starts to work`);
    }
    get name(){
        return `Your name is ${this.name}`
    }
    set name(newName){
        this.name = `[new]: ${newName}`;
    }
}

const humanData = {name: 'Bill', age: 23, gender: 'male'};
const billHuman = new Human(humanData);

Human.startWork();
billHuman.startWork();
console.log(billHuman);

class SuperHero extends Human{
    constructor({superpower, ...humanStuff}){
        super(humanStuff);
        this._superpower = superpower;
    }
    static startWork(){
        console.log('Ok, Justice Liague to the call!');
    }
    startWork(){
        console.log(`Ok, ${this._name} starts to work, and save the world`);
    }
}

const batmanData = {name: 'Bruce', age: 30, gender: 'male', superpower: 'money'};
const batman = new SuperHero(batmanData);

console.log(batman);
SuperHero.startWork();
console.log(batman.startWork());