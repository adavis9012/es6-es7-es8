// WeakMaps have garbage collector but it lost all cool methods of regular Map
let contacts = {
    users: [{
            "name": "bob",
            "number": "123-4567"
        },
        {
            "name": "ed",
            "number": "529-4567"
        },
        {
            "name": "sam",
            "number": "123-1149"
        },
        {
            "name": "al",
            "number": "189-4567"
        },
    ]
};

let weakMap = new WeakMap();

setEntries = contacts => {
    contacts.users.forEach(user => {
        weakMap.set(user, "stuff");
    });
}
setEntries(contacts);

console.log(weakMap.get(contacts.users[0]));
contacts.users[0] = null;
console.log(weakMap.get(contacts.users[0]));