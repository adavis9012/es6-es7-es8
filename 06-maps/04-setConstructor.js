//It seems like an array but, but it has other functions
// It can hold any data type
// All elems are unique
// Iterable (insertion order
let firstSet = new Set(['a', 'b'])
console.log('firstSet:', firstSet);
console.log('firstSet.entries():', firstSet.entries());
firstSet.delete('a');
console.log('firstSet:', firstSet);
console.log("firstSet.has('b')", firstSet.has('b'))

let secondSet = new Set();
secondSet.add({'name': 'david'});
secondSet.add({'name': 'cathe'});
console.log('secondSet:', secondSet);
console.log(secondSet.has({'name': 'david'}))
const obj = {'name': 'bob'};
secondSet.add(obj);
console.log(secondSet.has(obj)); //Check if has reference