const myContacts = new Map();
myContacts.set('Rob', '123-456789'); //Set
console.log(myContacts.get('Rob'));

let keyFunc = () => {
    console.log('Hello, World!');
};
myContacts.set(keyFunc, '789-12346'); // It works even with functions :o
console.log(myContacts.get(keyFunc)); //Get

console.log('Size:', myContacts.size); //Size
console.log('Entries:', myContacts.entries()); //Entries
console.log('Keys:', myContacts.keys()); //Keys
console.log('Values:', myContacts.values()); //Values

console.log('for of:') //For of works with maps, but not with objects
for (const val of myContacts) {
    console.log(val);
}

console.log('forEach:'); //ForEach works as well
myContacts.forEach((el, i) => {
    console.log(i,':', el);
});