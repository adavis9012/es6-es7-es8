const myContacts = new Map();
myContacts.set('Rob', '123-456789');
console.log(myContacts.get('Rob'));

let keyFunc = () => {
    console.log('Hello, World!');
};
myContacts.set(keyFunc, '789-12346'); // It works even with functions :o
console.log(myContacts.get(keyFunc));