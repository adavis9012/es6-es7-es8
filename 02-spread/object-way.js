const reducer = (state, action) => {
    // let newState = Object.assign({}, state);
    let newState = {...state}; // Makes a copy of state
    newState.newProperty = action.payload;
    console.log(newState);
    console.log(state);
    return newState;
}
const currState = {
    a: 1,
    b: 2,
    c: 3,
};
const action = {
    type: 'done',
    payload: 30,
};
reducer(currState, action);