const sum1 = (...numbs) => {
    return numbs.reduce((total, num) => total + num);
}
const numbs1 = [1,2,3,4,5,6,7];
const numbs2 = [1,2,3,4,5];
const numbs3 = [1,2,3,4];

const result1 = sum1(...numbs1);
console.log('result1:', result1);

const sum2 = (a, b, c, d, e) => {
    return a + b + c + d + e;
}
const result2More = sum2(...numbs1);
console.log('result2More:', result2More);

const result2Exact = sum2(...numbs2);
console.log('result2Exact:', result2Exact);

const result2Less = sum2(...numbs3);
console.log('result2Less:', result2Less);