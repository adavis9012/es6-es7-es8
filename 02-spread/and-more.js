const numbs = [2002,91,9412,694165120,6149,196498,4361,464196,84];
const min = Math.min(...numbs);
console.log('min:', min);

const others = ['a', 'b', 'c'];
const myArray = [1,2,3,4,...others,5,6,7,8];
console.log(myArray);

const array1 = [1,2,3,4,5];
// const array2 = array1.slice();
const array2 = [...array1];
array2.push(6);
console.log('array1:', array1);
console.log('array2:', array2);