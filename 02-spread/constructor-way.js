function Ball(radius, x, y) {
    this.r = radius;
    this.x = x;
    this.y = y;
}

let ballArgs = [50, 100, 100];

const ball1 = new Ball(ballArgs[0], ballArgs[1], ballArgs[2]); // ugly way
console.log(ball1);

const ball2 = Ball.apply(new Ball(),ballArgs); //doesn't work
console.log(ball2);

const ball3 = new Ball(...ballArgs); // Pretty way
console.log(ball3);