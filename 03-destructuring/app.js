const data = require('./data.json');

// First application
const {name, gender} = data;

function sayHello({name, gender}){
    return `Hello ${name} all ${gender} are welcome!`;
}
console.log(sayHello({gender, name}));

// Second application
const {tags: {[0]: first, ...others}} = data;

// console.log(tags); // It returns error due tags is just a destructure
console.log(first, others);


//Third Use

const {friends: {[0]: {name: firstFriend} }} = data;

console.log(firstFriend);