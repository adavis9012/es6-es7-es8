const x = Array.from('David');
console.log(x);

let lorem = 'Lorem ipsum dolor sit amet'
let phasellus = 'Phasellus sit amet nunc'
let nunc = 'Nunc non pulvinar felis';

let lines = Array.from([lorem, phasellus, nunc], (line) => {
    return `<li>${line}</line>`;
});
console.log(lines);

let numbers = Array.from([2,3,4], num => num+num);
console.log(numbers);