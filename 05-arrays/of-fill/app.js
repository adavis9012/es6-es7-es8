const arrayOf1 = Array.of(7);
console.log(arrayOf1);

const arrayOf2 = [7];
console.log(arrayOf2);

const arrayOf3 = Array(7);
console.log(arrayOf3);

// Fill
let arrFill1 = ['a','b', 'c', 'd', 'e'];
arrFill1.fill('x',3, 10);
console.log('arrFill1:', arrFill1);

let arrFill2 = ['a','b', 'c', 'd', 'e'];
arrFill2.fill('x', -3);
console.log('arrFill2:', arrFill2);

let arrFill3 = ['a','b', 'c', 'd', 'e'];
arrFill3.fill('x', -3, -1);
console.log('arrFill3:', arrFill3);

//Splice
let arrSplice = ['jan', 'feb', 'jun', 'oct'];
arrSplice.splice(3,0,'Aug', 'Aug', 'Aug');
console.log(arrSplice);