const arr = [1,2,3];

for (let i = 0; i < arr.length; i++) {
    const el = arr[i];
    console.log(el);
}
console.log('/*****************/')
for (const key in arr) {
    if (arr.hasOwnProperty(key)) {
        const el = arr[key];
        console.log(el);
    }
}
console.log('/*****************/')
for (const el of arr) {
    console.log(el);
}
console.log('/*****************/')
arr.forEach((el, i) => {
    console.log('el:', el);
    console.log('i:', i);
});

console.log('/****STRINGS******/')

const string = 'Lorem ipsum';
for (const key in string) {
    console.log(key);
}
console.log('/*****************/')
for (const val of string) {
    console.log(val);
}